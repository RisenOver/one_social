import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Textfield extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
            padding: EdgeInsets.symmetric(vertical: 50.0, horizontal: 0.0),
            child: Form(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    //Expanded( child:// ),
                    SizedBox(height: 20.0),
                    Container(
                      padding:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 35.0),
                      child: TextFormField(
                        decoration: InputDecoration(
                          labelText: 'Text Field', //Champ pour entrer l'Email
                          hintText: 'Write here',
                          hintStyle: TextStyle(fontSize: 16,),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: BorderSide(
                                width: 5,
                                style: BorderStyle.none,
                              )),
                        ),
                      ),
                    ),
                  ],
                )
            )
        ),
      ),
    );
  }
}
