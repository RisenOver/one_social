import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Button_purple extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
            padding: EdgeInsets.symmetric(vertical: 50.0, horizontal: 0.0),
            child: Form(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    //Expanded( child:// ),
                    SizedBox(height: 10.0),
                    Container(
                      padding:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 35.0),
                      child: FlatButton(
                        onPressed: () {},
                        color: Colors.deepPurple[300],
                        height: 60.0,
                        child: Text('BUTTON',// component button
                            style: TextStyle(
                                fontFamily: 'Roboto Bold',
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 20)),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(60.0)),
                      ),
                    ),
                  ],
                )
            )
        ),
      ),
    );
  }
}
