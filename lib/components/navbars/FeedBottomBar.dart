import 'package:flutter/material.dart';

class FeedBottomBar extends StatefulWidget {
  @override
  _FeedBottomBarState createState() => _FeedBottomBarState();
}

class _FeedBottomBarState extends State<FeedBottomBar> {
  // Declare variables here
  int _selectedIndex = 0;
  static const List<Widget> _widgetOptions = <Widget>[
    Text('Index 0: Search'),
    Text('Index 1: Network'),
    Text('Index 2: Alert'),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
         child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.search),
              label: 'Search',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.rss_feed_rounded ),
            label: 'Network',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.notifications),
            label: 'Alert',
          ),
        ],
        currentIndex: _selectedIndex,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        selectedItemColor: Colors.purple[300],
        onTap: _onItemTapped,
      ),
    );
  }
}