import "package:flutter/material.dart";

class FeedAppBar extends StatefulWidget {
  @override
  _FeedAppBarState createState() => _FeedAppBarState();
}

class _FeedAppBarState extends State<FeedAppBar> {
  // Declare variables here
  @override
  Widget build(BuildContext context) {
    runApp(MaterialApp(
      home: Scaffold(
        appBar: AppBar(
            title: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Image.asset('lib/assets/os.png',
                  fit: BoxFit.cover, height: 32),
            ]),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.account_circle_outlined),
              tooltip: 'Account Icon',
              color: Colors.blueGrey[400],
              onPressed: () {},
            ), //Icon
          ], //<Widget>[]
          backgroundColor: Colors.white,
          elevation: 5.0,
          leading: IconButton(
            icon: Icon(Icons.settings),
            tooltip: 'Setting Icon',
            color: Colors.blueGrey[400],
            onPressed: () {},
          ), //IconButton
          brightness: Brightness.dark,
        ), //AppBar
      ), //Scaffold
      debugShowCheckedModeBanner: false, //Removing Debug Banner
    )); //MaterialApp
  }
}

class _widgetOptions {
  static elementAt(int selectedIndex) {}
}

