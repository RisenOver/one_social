import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myonesocial/views/signup/partiebas.dart';
import 'views/Authentication/PasswordReset.dart';
import 'views/Authentication/Authentication.dart';
import 'views/settings/UserProfile.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  final Future<FirebaseApp> _initialization = Firebase.initializeApp();

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      initialRoute: "/",
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      // Ici seront mis toutes les routes de l'application
      // il suffit d'ajouter dans le premier paramètre de GetPage (name) le nom
      // de la route et en second paramètre (page) le nom du composant associé
      // Example : GetPage(name: "/le-nom-de-la-route", page: () => NomDuComposant()),
      getPages: [
        GetPage(name: "/", page: () => MyHomePage()),
        GetPage(name: "/user-profile", page: () => PartiEnd()),
        GetPage(name: "/password-reset", page: () => PasswordReset()),
      ],
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        bottomNavigationBar: Row(
          children: [
            Container(
              height: 60,
              width: MediaQuery.of(context).size.width / 1,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('lib/assets/footer.png'),
                      fit: BoxFit.cover)),
            )
          ],
        ),
        body: Authentication()
    );
  }
}
