import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:shared_preferences/shared_preferences.dart';

FirebaseApp secondaryApp = Firebase.app();

Future<void> alreadySignIn() async {
  await Firebase.initializeApp();

  FirebaseAuth auth = FirebaseAuth.instance;
  if (auth.currentUser != null) {
    print("Déjà connecté");
  } else {
    print("Pas connecté");
  }
}

Future<void> signOut() async {
  await Firebase.initializeApp();
  try {
    final SharedPreferences pref =
        await SharedPreferences.getInstance(); // acces to preferences
    pref.remove('email'); // remove préférences
    await FirebaseAuth.instance.signOut();
    print("Deconnecté");
  } catch (e) {
    print(e.toString());
  }
}

Future<dynamic> getData() async {
  final QuerySnapshot result =
      await FirebaseFirestore.instance.collection('users').get();
  final List<DocumentSnapshot> documents = result.docs;
  documents.forEach((data) => print(data.get('twi_id')));
}

Future<void> signIn(String email, String password, bool remember) async {
  await Firebase.initializeApp();

  try {
    await FirebaseAuth.instance
        .signInWithEmailAndPassword(email: email, password: password);

    if (remember == true) {
      String getEmail = email;
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('email', getEmail);
    }
  } on FirebaseAuthException catch (e) {
    if (e.code == 'user-not-found') {
      print('No user found for that email.');
    } else if (e.code == 'wrong-password') {
      print('Wrong password provided for that user.');
    }
  }
}

Future<List> getUsername(username) async {
  var arrayData = [];
  final QuerySnapshot result = await FirebaseFirestore.instance
      .collection('users')
      .where('user_username', isEqualTo: username)
      .get();
  final List<DocumentSnapshot> documents = result.docs;
  documents.forEach((data) => arrayData.add(data.get('user_username')));

  return (arrayData);
}
Future<List> getId(email) async {
  var arrayData = [];
  final QuerySnapshot result = await FirebaseFirestore.instance
      .collection('users')
      .where('user_email', isEqualTo: email)
      .get();
  final List<DocumentSnapshot> documents = result.docs;
  documents.forEach((data) => arrayData.add(data.id));

  return (arrayData);
}

Future<List> getEmail(email) async {
  var arrayData = [];
  final QuerySnapshot result = await FirebaseFirestore.instance
      .collection('users')
      .where('user_email', isEqualTo: email)
      .get();
  final List<DocumentSnapshot> documents = result.docs;
  documents.forEach((data) => arrayData.add(data.get('user_email')));

  return (arrayData);
}

Future<dynamic> insertDataTwi(session) async {
  CollectionReference users =
      FirebaseFirestore.instance.collection('users'); //Write in database
  users.add({
    'twi_id': session.userId,
    'twi_username': session.username,
    'twi_token': session.token,
    'twi_secret_token': session.secret
  });
}

Future<dynamic> insertDataOS(user, password, email) async {
  CollectionReference users =
      FirebaseFirestore.instance.collection('users'); //Write in database
  users.add({
    'user_username': user,
    'user_password': password,
    'user_email': email,
  });
}

Future<User> getCurrentUser() async {
  User user = await FirebaseAuth.instance.currentUser;
  return user;
}

Future<void> sendEmailVerification() async {
  User user = await FirebaseAuth.instance.currentUser;
  user.sendEmailVerification();
}

Future<bool> isEmailVerified() async {
  User user = await FirebaseAuth.instance.currentUser;
  return user.emailVerified;
}

Future<void> sendPasswordReset(String email) async {
  FirebaseAuth.instance.sendPasswordResetEmail(email: email);
}

Future<List> getDataFromEmail(email) async {
  var arrayData = [];
  final QuerySnapshot result = await FirebaseFirestore.instance
      .collection('users')
      .where('user_email', isEqualTo: email)
      .get();
  final List<DocumentSnapshot> documents = result.docs;
  documents.forEach((data) => arrayData.add(data.data()));

  return (arrayData);
}

Future<List> UpdateDataUser(uid, data) async {
  FirebaseFirestore.instance
      .doc('users/$uid')
      .update({
    "user_username":data[0]['user_username'],
    "genre":data[0]['genre'],
    "nom":data[0]['nom'],
    "prenom":data[0]['prenom'],
    "portable":data[0]['portable']
  }).then((result){
    print("new USer true");
  }).catchError((onError){
    print("onError");
  });

}
