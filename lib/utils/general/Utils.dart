import '../apis/ApiFirebase.dart' as ApiFirebase;

validateUsername(username)async {

  List data = await ApiFirebase.getUsername(username);
  print(data);
  if(data.length > 0) {
    return false;
  }else{
    return true;
  }

}

validateEmail(email)async {

  List data = await ApiFirebase.getEmail(email);
  print(data);
  if(data.length > 0) {
    return false;
  }else{
    return true;
  }

}