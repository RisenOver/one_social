import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class AjoutRS extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      bottomNavigationBar: Row(
        children: <Widget> [
          Container(//Insertion du footer
            padding:
            EdgeInsets.symmetric(vertical: 30.0, horizontal: 55.0),
            width: MediaQuery.of(context).size.width/1,
            child: FlatButton(//Ajout du bouton APPLIQUER
              onPressed: () {},
              color: Colors.deepPurple[300],
              height: 60.0,
              child: Text('APPLIQUER', // Ce bout de code correspond au bouton APPLIQUER
                  style: TextStyle(
                      fontFamily: 'Roboto Bold',
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20)),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(70.0)),
            ),
          ),
        ],
      ),
      body: ListView(
        padding:
        EdgeInsets.symmetric(vertical: 60.0, horizontal: 20.0),
        children: <Widget> [
          Container(
              padding:
              EdgeInsets.symmetric(vertical: 20.0, horizontal: 0.0),
              child: Text(
                'Lier vos réseaux sociaux :',//Ajout du sous-titre
                style: TextStyle(
                  fontFamily: 'Roboto Light',
                  color: Color(0xFF14171A),
                  fontSize: 20,
                ),
                //borderRadius: new BorderRadius.circular(10.0),
              )),
          //Ajout des cadres en fonction des Réseaux sociaux connectés
          Card(
            elevation: 4,
            // children: <Widget>[
            child: ListTile(
                leading: Icon(FontAwesomeIcons.reddit, color: Colors.orange, size: 30),
                title: Text('Reddit',
                  style: TextStyle(
                    fontFamily: 'Roboto Light',
                    color: Color(0xFF14171A),
                    fontSize: 20,
                  ) ,),
                trailing: Wrap(
                  spacing: 12, // space between two icons
                  children: <Widget>[
                    Icon(Icons.brightness_1_rounded, color: Color(0xFF139933), size: 15,),//code correspond au bouton  couleur vert pastille lorsque le réseau socail est connecté
                  ],
                ),
                // shape: new CircleBorder(),
                onTap: () => print("ListTile")
            ),
          ),
          Card(
            elevation: 4,
            // children: <Widget>[
            child: ListTile(
                leading: Icon(FontAwesomeIcons.youtube, color: Colors.red, size: 30),
                title: Text('Youtube',
                  style: TextStyle(
                    fontFamily: 'Roboto Light',
                    color: Color(0xFF14171A),
                    fontSize: 20,
                  ),),
                trailing: Wrap(
                  spacing: 12, // space between two icons
                  children: <Widget>[
                    Icon(Icons.brightness_1_rounded, color: Color(0xFF139933), size: 15,),//code correspond au bouton couleur vert pastille lorsque le réseau socail est connecté
                  ],
                ),
                // shape: new CircleBorder(),
                onTap: () => print("ListTile")
            ),),
          //Ajout des cadres en fonction des Réseaux sociaux (Réseaux sociaux non connectés)
          Card(
            // children: <Widget>[
            child: ListTile(
                leading: Icon(FontAwesomeIcons.facebook, color: Color.fromRGBO(66, 103, 178, 1), size: 30),
                title: Text('Facebook',
                  style: TextStyle(
                    fontFamily: 'Roboto Light',
                    color: Colors.grey,
                    fontSize: 20,
                  ),
                ),
                trailing: Icon(Icons.brightness_1_rounded, color: Colors.red, size: 15,), //code correspond au bouton  couleur rouge lorsque le réseau socail est non connecté
                //  shape: new CircleBorder(),
                onTap: () => print("ListTile")
            ),),
          Card(
            // children: <Widget>[
            child: ListTile(
                leading: Icon(FontAwesomeIcons.twitter, color: Colors.blueAccent, size: 30),
                title: Text('Twitter',
                  style: TextStyle(
                    fontFamily: 'Roboto Light',
                    color: Colors.grey,
                    fontSize: 20,
                  ),),
                trailing: Wrap(
                  spacing: 12, // space between two icons
                  children: <Widget>[
                    Icon(Icons.brightness_1_rounded, color: Colors.red, size: 15,),//code correspond au bouton  couleur rouge lorsque le réseau socail est non connecté
                  ],
                ),
                // shape: new CircleBorder(),
                onTap: () => print("ListTile")
            ),),
          Card(
            // children: <Widget>[
            child: ListTile(
                leading: Icon(FontAwesomeIcons.instagram, color: Color.fromRGBO(138, 58, 185, 1), size: 30),
                title: Text('Instagram',
                  style: TextStyle(
                    fontFamily: 'Roboto Light',
                    color: Colors.grey,
                    fontSize: 20,
                  ),),
                trailing: Wrap(
                  spacing: 12, // space between two icons
                  children: <Widget>[
                    Icon(Icons.brightness_1_rounded, color: Colors.red, size: 15,),//code correspond au bouton  couleur rouge lorsque le réseau socail est non connecté
                  ],
                ),
                onTap: () => print("ListTile")
            ),),
          Card(
            // children: <Widget>[
            child: ListTile(
                leading: Icon(FontAwesomeIcons.pinterest, color: Colors.red, size: 30),
                title: Text('Pinterest',
                  style: TextStyle(
                    fontFamily: 'Roboto Light',
                    color: Colors.grey,
                    fontSize: 20,
                  ),),
                trailing: Wrap(
                  spacing: 12, // space between two icons
                  children: <Widget>[
                    Icon(Icons.brightness_1_rounded, color: Colors.red, size: 15,),//code correspond au bouton  couleur rouge lorsque le réseau socail est non connecté
                  ],
                ),
                // shape: new CircleBorder(),
                onTap: () => print("ListTile")
            ),),
          Card(
            // children: <Widget>[
            child: ListTile(
                leading: Icon(FontAwesomeIcons.linkedin, color: Color.fromRGBO(0, 119, 181, 1), size: 30),
                title: Text('Linkedin',
                  style: TextStyle(
                    fontFamily: 'Roboto Light',
                    color: Colors.grey,
                    fontSize: 20,
                  ),),
                trailing: Wrap(
                  spacing: 12, // space between two icons
                  children: <Widget>[
                    Icon(Icons.brightness_1_rounded, color: Colors.red, size: 15,),//code correspond au bouton  couleur rouge lorsque le réseau socail est non connecté
                  ],
                ),
                // shape: new CircleBorder(),
                onTap: () => print("ListTile")
            ),),
          Card(
            // children: <Widget>[
            child: ListTile(
                leading: Icon(FontAwesomeIcons.twitch, color: Color.fromRGBO(100, 65, 165, 1), size: 30),
                title: Text('Twitch',
                  style: TextStyle(
                    fontFamily: 'Roboto Light',
                    color: Colors.grey,
                    fontSize: 20,
                  ),),
                trailing: Wrap(
                  spacing: 12, // space between two icons
                  children: <Widget>[
                    Icon(Icons.brightness_1_rounded, color: Colors.red, size: 15,),//code correspond au bouton  couleur rouge lorsque le réseau socail est non connecté
                  ],
                ),
                // shape: new CircleBorder(),
                onTap: () => print("ListTile")
            ),
          ),

        ],
      ),
    );
  }
}

