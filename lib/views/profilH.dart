import 'package:flutter/material.dart';

class ProfilHaute extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: getAppBar(),
      body: getBody(size),
    );
  }

  Widget getAppBar() {
    return PreferredSize(
      preferredSize: Size.fromHeight(55),
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(left: 10, right: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  SizedBox(width: 10),
                ],
              ),
              Row(
                children: [
                  Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.deepPurple[300],
                      ),
                      child: Center(
                        child: IconButton(
                          icon: Icon(Icons.share_outlined,  color: Colors.white),
                          onPressed: () {} , // Ce bout de code correspond au bouton partager qui conduira au code barre
                        ),
                      )

                  )],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget getBody(size) {
    return ListView(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 10, right: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Container(
                    width: (size.width - 20) * 0.3,
                    child: Stack(
                      children: [
                        Container(
                          height: 80,
                          width: 80,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image: NetworkImage("https://pbs.twimg.com/profile_images/916384996092448768/PF1TSFOE_400x400.jpg"), //c'est ici que doit figurer la photo de profil de l'utilisateur
                                  fit: BoxFit.cover
                              )
                          ),
                        ),
                        Positioned(
                          top: 0,
                          right: 15,
                          child: Container(
                            height: 30,
                            width: 35,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.deepPurple[300],
                            ),
                            child: Center(
                              child: IconButton(
                                  icon: Icon(Icons.edit_outlined, color: Colors.white, size: 18),
                                onPressed: () {} ,//Ce code correspond à la modification du profil (photo)
                            ),
                            )
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: (size.width - 20) * 0.7,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Column(
                          children: [
                            Text(
                              "2", // Statistiques pour le nombre de publications faites par l'utilisateur
                              style: TextStyle(fontSize: 18,  fontFamily: 'Roboto', fontWeight: FontWeight.bold, color: Colors.deepPurple),
                            ),
                            Text(
                              "Publications",
                              style: TextStyle(fontSize: 16,  fontFamily: 'Roboto'),
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            Text(
                              "23", // Statistiques pour le nombre d'abonnés de l'utilisateur
                              style: TextStyle(fontSize: 18,  fontFamily: 'Roboto', fontWeight: FontWeight.bold, color: Colors.deepPurple),
                            ),
                            Text(
                              "Abonnés",
                              style: TextStyle(fontSize: 16,  fontFamily: 'Roboto'),
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            Text(
                              "42", // Statistiques pour le nombre d'abonnements de l'utilisateur
                              style: TextStyle(fontSize: 18,  fontFamily: 'Roboto', fontWeight: FontWeight.bold, color: Colors.deepPurple),
                            ),
                            Text(
                              "Abonnements",
                              style: TextStyle(fontSize: 16,  fontFamily: 'Roboto'),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),

            ],
          ),
        ),

      ],
    );
  }
}


