import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myonesocial/controllers/PasswordResetController.dart';

class PasswordReset extends StatelessWidget {

  final PasswordResetController passwordResetController = Get.put(PasswordResetController());
  //TextEditingController identifiantControllers = new TextEditingController();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
            padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
            child: Form(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        color: Color(0xfff5f8fa),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(0),
                            topRight: Radius.circular(0),
                            bottomLeft: Radius.circular(0),
                            bottomRight: Radius.circular(0)),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey.withOpacity(0.1),
                              spreadRadius: 5,
                              offset: Offset(0.0, 0.75)
                          ),
                        ],
                      ),
                      child: Image.asset('lib/assets/os.png', //Insertion du logo
                          height: 250.0, width: 250.0),
                    ),
                    Container(
                        decoration: new BoxDecoration(color: Color(0xfff5f8fa),),
                        child: Text(
                          'ONESOCIAL', textAlign: TextAlign.center,
                          style: TextStyle(
                            shadows: [
                              Shadow(
                                blurRadius: 10.0,
                                color: Color.fromARGB(50, 20, 23, 26),
                                offset: Offset(5.0, 5.0),
                              ),
                            ],
                            fontFamily: 'Roboto Regular',
                            color: Colors.deepPurple[300],
                            fontSize: 36,
                            fontWeight: FontWeight.w300,
                          ),
                        )),
                    Container(
                      padding:
                      EdgeInsets.symmetric(vertical: 30.0, horizontal: 40.0),
                      decoration: BoxDecoration(
                        color: Color(0xfff5f8fa),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(0),
                            topRight: Radius.circular(0),
                            bottomLeft: Radius.circular(0),
                            bottomRight: Radius.circular(0)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.2),
                            offset: Offset(15.0, 20.0),
                            blurRadius: 20.0,
                          ),
                        ],
                      ),
                      child: Center(
                        child:  Text('MOT DE PASSE OUBLIÉ',
                          style: TextStyle(
                              shadows: [
                                Shadow(
                                  blurRadius: 10.0,
                                  color: Color.fromARGB(50, 20, 23, 26),
                                  offset: Offset(5.0, 5.0),
                                ),
                              ],
                              color: Colors.black,
                              fontFamily: 'Roboto Regular',
                              fontWeight: FontWeight.bold,
                              fontSize: 20.0),
                        ),
                      ),
                    ),
                    SizedBox(height: 30.0),
                    Container(
                        padding:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 30.0),
                        child:  Text('Un Email vous sera envoyé afin de récupérer votre mot de passe.',textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.deepPurple[300],
                              fontFamily: 'Roboto Regular',
                              fontWeight: FontWeight.w600,
                              fontSize: 15.0),
                        )
                    ),
                    SizedBox(height: 20.0),
                    Container(
                      padding:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 30.0),
                      child: TextFormField(
                        controller: passwordResetController.getEmail,
                        decoration: InputDecoration(
                          labelText: 'Email', //Champ pour entrer l'Email
                          hintText: 'Entrez votre adresse Email',
                          hintStyle: TextStyle(fontSize: 16,),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: BorderSide(
                                width: 5,
                                style: BorderStyle.none,
                              )),
                        ),
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Container(
                      padding:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 90.0),
                      child: FlatButton(
                        onPressed: () {
                          passwordResetController.sendPasswordReset();
                        },
                        color: Colors.deepPurple[300],
                        height: 60.0,
                        child: Text('ENVOYER', //Ajout du bouton Envoyer
                            style: TextStyle(
                                fontFamily: 'Roboto Bold',
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 20)),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(60.0)),
                      ),
                    ),
                    SizedBox(height: 180.0),
                    Container(
                        child: Row(
                          children: <Widget>[
                            Text('Tu n\'as pas de compte ?',
                              style: TextStyle(
                                fontSize: 13,
                                fontFamily: 'Roboto Regular',
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                              ),),
                            FlatButton(
                              textColor: Colors.deepPurple[300],
                              child: Text(
                                'INSCRIPTION',
                                style: TextStyle(
                                  fontSize: 13,
                                  fontFamily: 'Roboto Regular',
                                ),
                              ),
                              onPressed: () {
                                //Lien renvoyant vers l'inscription
                                Get.toNamed('/');
                              },
                            )
                          ],
                          mainAxisAlignment: MainAxisAlignment.center,
                        )),
                  ],
                )
            )
        ),
      ),
    );
  }
}
