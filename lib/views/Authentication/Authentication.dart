import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myonesocial/controllers/AuthenticationController.dart';

import 'SignIn.dart';
import 'SignUp.dart';

class Authentication extends StatelessWidget {

  final AuthenticationController authenticationController = Get.put(AuthenticationController());


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.symmetric(vertical: 30.0, horizontal: 0.0),
              child:
              Image.asset('lib/assets/os.png', height: 180.0, width: 180.0),
            ),
            Container(
              child: Center(
                child: Text(
                  "ONESOCIAL",
                  style: TextStyle(
                      shadows: [
                        Shadow(
                          blurRadius: 10.0,
                          color: Color.fromARGB(50, 20, 23, 26),
                          // color: Colors.grey.withOpacity(0.3),
                          offset: Offset(5.0, 5.0),
                        ),
                      ],
                      fontFamily: 'Roboto Light',
                      color: Colors.deepPurple[300],
                      fontSize: 36),
                ),
              ),
            ),
            Container(
              child: TabBar(
                unselectedLabelColor: Colors.grey,
                labelColor: Colors.black,
                unselectedLabelStyle: TextStyle(
                  fontSize: 20.0,
                  fontFamily: 'Roboto Regular',
                ),
                labelStyle: TextStyle(
                  fontSize: 20.0,
                  fontFamily: 'Roboto bold',
                  fontWeight: FontWeight.bold,
                ),
                tabs: [
                  Tab(
                    text: 'CONNEXION',
                  ),
                  Tab(
                    text: 'INSCRIPTION',
                  )
                ],
                controller: authenticationController.getTabController,
                indicatorSize: TabBarIndicatorSize.tab,
              ),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(0),
                    topRight: Radius.circular(0),
                    bottomLeft: Radius.circular(0),
                    bottomRight: Radius.circular(0)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.2),
                    offset: Offset(15.0, 20.0),
                    blurRadius: 20.0,
                  ),
                ],
              ),
            ),
            Expanded(
              child: TabBarView(
                children: <Widget>[
                  SignIn(),
                  SignUp(),
                ],
                controller: authenticationController.getTabController,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
