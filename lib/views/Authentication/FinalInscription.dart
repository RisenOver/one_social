import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class FinalInscription extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      bottomNavigationBar: Row(
        children: <Widget> [
          Container(//Insertion du footer
            padding:
            EdgeInsets.symmetric(vertical: 30.0, horizontal: 35.0),
            width: MediaQuery.of(context).size.width/1,
            child: FlatButton(//Ajout du bouton finaliser mon inscription
              onPressed: () {},
              color: Colors.deepPurple[300],
              height: 50.0,
              child: Text('FINALISER MON INSCRIPTION',
                  style: TextStyle(
                      fontFamily: 'Roboto Bold',
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20)),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(90.0)),
            ),
          ),
        ],
      ),
      body: ListView(
        padding:
        EdgeInsets.symmetric(vertical: 60.0, horizontal: 20.0),
        children: <Widget> [
          Text(
            'BIENVENUE XDIABLOX9 SUR', textAlign: TextAlign.center,//Ajout du titre ainsi que le pseudo de l'utilisateur OneSocial
            style: TextStyle(
              shadows: [
                Shadow(
                  blurRadius: 10.0,
                  color: Color.fromARGB(60, 20, 23, 26),
                  // color: Colors.grey.withOpacity(0.3),
                  offset: Offset(5.0, 5.0),
                ),
              ],
              fontFamily: 'Roboto Regular',
              color: Colors.deepPurple[300],
              fontSize: 20,
            ),
            //borderRadius: new BorderRadius.circular(10.0),
          ),
          Container(
              child: Text('ONESOCIAL', textAlign: TextAlign.center,
                style: TextStyle(
                  shadows: [
                    Shadow(
                      blurRadius: 10.0,
                      color: Color.fromARGB(50, 20, 23, 26),
                      // color: Colors.grey.withOpacity(0.3),
                      offset: Offset(5.0, 5.0),
                    ),
                  ],
                  fontFamily: 'Roboto Light',
                  color: Colors.deepPurple[300],
                  fontSize: 36,
                ),
                //borderRadius: new BorderRadius.circular(10.0),
              )),
          SizedBox(height: 40),
          Container(
              padding:
              EdgeInsets.symmetric(vertical: 20.0, horizontal: 0.0),
              child: Text(
                'Lier vos réseaux sociaux :',//Ajout du sous-titre
                style: TextStyle(
                  fontFamily: 'Roboto Light',
                  color: Color(0xFF14171A),
                  fontSize: 20,
                ),
                //borderRadius: new BorderRadius.circular(10.0),
              )),
          //Ajout des cadres en fonction des Réseaux sociaux connectés
          Card(
            elevation: 4,
            // children: <Widget>[
            child: ListTile(
                leading: Icon(FontAwesomeIcons.reddit, color: Colors.orange),
                title: Text('Reddit'),
                trailing: Wrap(
                  spacing: 12, // space between two icons
                  children: <Widget>[
                    Icon(Icons.brightness_1_rounded, color: Colors.green, size: 15,),// icon-1
                  ],
                ),
                // shape: new CircleBorder(),
                onTap: () => print("ListTile")
            ),
          ),
          Card(
            elevation: 4,
            // children: <Widget>[
            child: ListTile(
                leading: Icon(FontAwesomeIcons.youtube, color: Colors.red),
                title: Text('Youtube'),
                trailing: Wrap(
                  spacing: 12, // space between two icons
                  children: <Widget>[
                    Icon(Icons.brightness_1_rounded, color: Colors.green, size: 15,),// icon-1
                  ],
                ),
                // shape: new CircleBorder(),
                onTap: () => print("ListTile")
            ),),
          Card(
            // children: <Widget>[
            child: ListTile(
                leading: Icon(FontAwesomeIcons.facebook, color: Color.fromRGBO(66, 103, 178, 1),),
                title: Text('Facebook',
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                ),
                trailing: Icon(Icons.brightness_1_rounded, color: Colors.red, size: 15,),
                //  shape: new CircleBorder(),
                onTap: () => print("ListTile")
            ),),
          Card(
            // children: <Widget>[
            child: ListTile(
                leading: Icon(FontAwesomeIcons.twitter, color: Colors.blueAccent),
                title: Text('Twitter',
                  style: TextStyle(
                    color: Colors.grey,
                  ),),
                trailing: Wrap(
                  spacing: 12, // space between two icons
                  children: <Widget>[
                    Icon(Icons.brightness_1_rounded, color: Colors.red, size: 15,),// icon-1
                  ],
                ),
                // shape: new CircleBorder(),
                onTap: () => print("ListTile")
            ),),
          Card(
            // children: <Widget>[
            child: ListTile(
                leading: Icon(FontAwesomeIcons.instagram, color: Color.fromRGBO(138, 58, 185, 1),),
                title: Text('Instagram',
                  style: TextStyle(
                    color: Colors.grey,
                  ),),
                trailing: Wrap(
                  spacing: 12, // space between two icons
                  children: <Widget>[
                    Icon(Icons.brightness_1_rounded, color: Colors.red, size: 15,),// icon-1
                  ],
                ),
                onTap: () => print("ListTile")
            ),),
          Card(
            // children: <Widget>[
            child: ListTile(
                leading: Icon(FontAwesomeIcons.pinterest, color: Colors.red),
                title: Text('Pinterest',
                  style: TextStyle(
                    color: Colors.grey,
                  ),),
                trailing: Wrap(
                  spacing: 12, // space between two icons
                  children: <Widget>[
                    Icon(Icons.brightness_1_rounded, color: Colors.red, size: 15,),// icon-1
                  ],
                ),
                // shape: new CircleBorder(),
                onTap: () => print("ListTile")
            ),),
          Card(
            // children: <Widget>[
            child: ListTile(
                leading: Icon(FontAwesomeIcons.linkedin, color: Color.fromRGBO(0, 119, 181, 1),),
                title: Text('Linkedin',
                  style: TextStyle(
                    color: Colors.grey,
                  ),),
                trailing: Wrap(
                  spacing: 12, // space between two icons
                  children: <Widget>[
                    Icon(Icons.brightness_1_rounded, color: Colors.red, size: 15,),// icon-1
                  ],
                ),
                // shape: new CircleBorder(),
                onTap: () => print("ListTile")
            ),),
          Card(
            // children: <Widget>[
            child: ListTile(
                leading: Icon(FontAwesomeIcons.twitch, color: Color.fromRGBO(100, 65, 165, 1),),
                title: Text('Twitch',
                  style: TextStyle(
                    color: Colors.grey,
                  ),),
                trailing: Wrap(
                  spacing: 12, // space between two icons
                  children: <Widget>[
                    Icon(Icons.brightness_1_rounded, color: Colors.red, size: 15,),// icon-1
                  ],
                ),
                // shape: new CircleBorder(),
                onTap: () => print("ListTile")
            ),
          ),

        ],
      ),
    );
  }
}

