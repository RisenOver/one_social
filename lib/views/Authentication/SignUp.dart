
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myonesocial/controllers/SignUpController.dart';


class SignUp extends StatelessWidget {

  final SignUpController signUpController = Get.put(SignUpController());

  @override
  Widget build(BuildContext context) {
   return Container(
       child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
          child: Form(
          key: signUpController.getFormKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                //Expanded( child:
                //Ajout des champs de saisies
                SizedBox(height: 10.0),
                Container(
                  padding:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 30.0),
                  child: Obx(() => TextFormField(
                    controller: signUpController.getUsernameController,
                    decoration: InputDecoration(
                      labelText: 'Identifiant',
                      hintText: 'Entrez votre identifiant',

                      hintStyle: TextStyle(fontSize: 16),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),
                          borderSide: BorderSide(
                            width: 5,
                            style: BorderStyle.none,
                          )),
                      ),
                      onChanged: (value) { signUpController.setName = value; },
                      validator: (value) {
                        Pattern username =
                            r'^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$';
                        RegExp regex = new RegExp(username);
                        if (!regex.hasMatch(value)) {
                          return 'Invalid username';
                        } else if (signUpController.getCheckUsername) {
                          return "username already exist";
                        } else {
                          return null;
                        }
                      }
                  ),
                  )),
                SizedBox(height: 10.0),
                Obx(() => Container(
                  padding:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 30.0),
                  child: TextFormField(
                    controller: signUpController.getPasswordController,
                    obscureText: signUpController.getIsHidden,
                    decoration: InputDecoration(
                      labelText: 'Mot de passe',
                      hintText: 'Entrez votre mot de passe',
                      hintStyle: TextStyle(fontSize: 16),
                      suffix: InkWell(
                        onTap : () {signUpController.togglePasswordView();},
                        child: Icon (
                          signUpController.getIsHidden
                              ? Icons.visibility
                              : Icons.visibility_off,
                        ),
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),
                          borderSide: BorderSide(
                            color: Colors.red,
                            width: 5,
                            style: BorderStyle.none,
                          )),
                    ),
                    validator: (value) {
                      Pattern password =
                          r'^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,15}$'; //Expression de correspondance de mot de passe. Le mot de passe doit comporter au moins 4 caractères, pas plus de 8 caractères, et doit inclure au moins une lettre majuscule, une lettre minuscule et un chiffre numérique.
                      RegExp regex = new RegExp(password);
                      if (!regex.hasMatch(value))
                        return 'Invalid password';
                      else
                        return null;
                    },
                   // obscureText: true,
                  ),
                )),
                SizedBox(height: 10.0),
                Container(
                  padding:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 30.0),

                  child: Obx(() => TextFormField(
                    controller: signUpController.getEmailController,
                    decoration: InputDecoration(
                      labelText: 'Email',
                      hintText: 'Entrez votre email',
                      hintStyle: TextStyle(fontSize: 16),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),
                          borderSide: BorderSide(
                            width: 5,
                            style: BorderStyle.none,
                          )),
                    ),
                      onChanged: (value) { signUpController.setEmail = value; },
                      validator: (value) {
                        if (!RegExp(
                            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                            .hasMatch(value))
                          return 'Invalid email';
                        else if (signUpController.getCheckEmail) {
                          return ("Email already exist");
                        } else
                          return null;
                      }
                  ),
                )),
                Container(
                    padding:
                        EdgeInsets.symmetric(vertical: 0.0, horizontal: 30.0),
                    child: Row(
                      children: [
                        GetBuilder<SignUpController>(
                          init: SignUpController(),
                          builder: (_) => Checkbox(
                            value: _.getCheckBoxValue,
                            onChanged: (bool value) {
                              _.changeCheckBoxValue(value);
                            })),
                        Text("J'accepte les",
                            style: TextStyle(
                              fontSize: 13,
                              fontFamily: 'Roboto Regular',
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                            )),
                        FlatButton(
                          textColor: Colors.deepPurple[300],
                          child: Text(
                            'Thèmes & Conditions',
                            style: TextStyle(
                              fontSize: 13,
                              fontFamily: 'Roboto Regular',
                            ),
                          ),
                          onPressed: () {
                            //Lien renvoyant vers l'inscription
                          },
                        )
                      ],
                    )),
                SizedBox(height: 10.0),
                //Ajout du button d'inscription
                 Container(
                  padding:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 30.0),
                  child: FlatButton(
                    height: 40.0,
                    color: Colors.deepPurple[300],
                    child: Text('INSCRIPTION',
                        style: TextStyle(
                            fontFamily: 'Roboto Bold',
                            color: Colors.white,
                            fontSize: 20)),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(60.0)),
                    onPressed: () {
                      signUpController.signUp();
                    },
                  ))
                ],
            ),
          ),
        ),
       )
   );
        }
}
