import 'package:flutter/material.dart';
import 'package:flutter_session/flutter_session.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:get/get.dart';
import 'package:myonesocial/controllers/SignInController.dart';
import 'package:myonesocial/utils/apis/ApiFirebase.dart' as ApiFirebase;

import 'PasswordReset.dart';

class SignIn extends StatelessWidget {
  //User user = FirebaseAuth.instance.currentUser;

  final SignInController signInController = Get.put(SignInController());

  @override
  Widget build(BuildContext context) {
    return Container(
        child: SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
        child: Form(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              SizedBox(height: 10.0),
              Container(
                // champ identifiant
                padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 30.0),
                child: TextFormField(
                  controller: signInController.getIdentifiantControllers,
                  decoration: InputDecoration(
                    labelText: 'Identifiant',
                    hintText: 'Entrez votre identifiant',
                    hintStyle: TextStyle(fontSize: 16),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30),
                        borderSide: BorderSide(
                          width: 5,
                          style: BorderStyle.none,
                        )),
                  ),
                ),
              ),
              SizedBox(height: 10.0),
              Container(
                // champ mot de passe
                padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 30.0),
                child: TextFormField(
                  controller: signInController.getPasswordControllers,
                  obscureText: signInController.getIsHidden,
                  decoration: InputDecoration(
                    labelText: 'Mot de passe',
                    hintText: 'Entrez votre mot de passe',
                    hintStyle: TextStyle(fontSize: 16),
                    suffix: InkWell(
                      onTap: () {signInController.setIsHidden = !signInController.getIsHidden;},
                      child: Icon(
                        signInController.getIsHidden ? Icons.visibility : Icons.visibility_off,
                      ),
                    ),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30),
                        borderSide: BorderSide(
                          color: Colors.red,
                          width: 5,
                          style: BorderStyle.none,
                        )),
                  ),
                ),
              ),
              Container(
                  margin: const EdgeInsets.only(left: 90.0, right: 20.0),
                  child: Row(
                    children: [
                      Text('Se souvenir de moi',
                          style: TextStyle(
                              fontFamily: 'Roboto Regular',
                              color: Colors.grey,
                              fontSize: 13)),
                      SizedBox(height: 10.0, width: 10.0),
                      Obx(() => FlutterSwitch(
                        width: 85.0,
                        height: 35.0,
                        valueFontSize: 17.0,
                        toggleSize: 20.0,
                        value: signInController.getCheckRemember,
                        borderRadius: 30.0,
                        padding: 8.0,
                        showOnOff: true,
                        activeColor: Colors.deepPurple[300],
                        inactiveColor: Colors.grey,
                        onToggle: (bool val) {
                          signInController.changeCheckMember(val);
                        },
                      ),
                      )],
                  )),
              SizedBox(height: 10.0),
              //Ajout du button d'inscription
              Container(
                padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 30.0),
                child:  Obx(() => FlatButton(
                  onPressed: () async {
                    var email = signInController.getIdentifiantControllers.text;
                    var password = signInController.getPasswordControllers.text;
                    var checkRemember = signInController.getCheckRemember;
                    await ApiFirebase.signIn(email, password, checkRemember);

                    //infoUserController.setEmail = email;
                    var session = FlutterSession();
                    await session.set("email", email);
                    await session.set("checkRemember", checkRemember);

                    Get.toNamed('/user-profile');
                    //ApiFirebase.signOut();
                    //final String userid = user.uid;
                    //print("Id de l'user :  $userid");
                  },
                  height: 40.0,
                  color: Colors.deepPurple[300],
                  child: Text('CONNEXION',
                      style: TextStyle(
                          fontFamily: 'Roboto Bold',
                          color: Colors.white,
                          fontSize: 20)),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(60.0)),
                ))),
              Container(
                child: Center(
                  // padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 50.0),
                  child: FlatButton(
                    textColor: Colors.deepPurple[300],
                    child: Text(
                      'Mot de passe oublié ?',
                      style: TextStyle(
                        fontSize: 13,
                        color: Colors.grey,
                        fontFamily: 'Roboto Regular',
                      ),
                    ),
                    onPressed: () {
                      Get.toNamed('/password-reset');
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    ));
  }
}

