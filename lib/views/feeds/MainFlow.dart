import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:myonesocial/components/navbars/FeedBottomBar.dart';
import 'package:myonesocial/components/navbars/FeedAppBar.dart';

class MainFlow extends StatefulWidget {
  @override
  _MainFlowState createState() => _MainFlowState();
}

class _MainFlowState extends State<MainFlow> with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: FeedAppBar(),
      ),
    );
  }
}