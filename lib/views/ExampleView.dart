/*
  * Obx(() => Text("${controller.name}"));
  * ou
  * GetX<ExampleController>(
  *    builder: (exampleController) {
  *      return Text('${exampleController.uneVariableDeExampleController.value}');
  *    },
  *  ),
  *
  * GetBuilder
 */
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myonesocial/controllers/ExampleController.dart';

class ExampleView extends StatelessWidget {

  ExampleController controller = Get.put(ExampleController());

  /*
  Utiliser les routes
  Utiliser les consumers (Obx, GetX et GetBuilder)
   */
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Mon appBar")),
      body: Container(
        child: Obx(() =>Text(controller.getNom))
      ),
    );
  }

}