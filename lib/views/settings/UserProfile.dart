import 'package:get/get.dart';
import 'package:flutter/material.dart';

import 'package:myonesocial/utils/apis/ApiFirebase.dart' as ApiFirebase;
import 'package:myonesocial/utils/general/Utils.dart' as UtilsFunction;

import 'package:crypto/crypto.dart';
import 'dart:convert'; // for the utf8.encode method

import 'package:firebase_auth/firebase_auth.dart';

import 'package:myonesocial/controllers/UserProfileController.dart';

import 'package:flutter/cupertino.dart';
import 'package:image_picker/image_picker.dart';

class UserProfile extends StatelessWidget {
  //data from database request
  final UserProfileController userProfileController = Get.put(UserProfileController());




  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Test")),
        body: Center(
          child: Column(children: <Widget>[
            GestureDetector(
                onTap: () async => await listPopUpChoice(context),
                child: CircleAvatar(
                  radius: 50,
                  child: Obx(() =>
                      CircleAvatar(
                        radius: 50,
                        backgroundImage: userProfileController.getImage != ''
                            ? NetworkImage(userProfileController.getImage)
                            :
                        AssetImage('lib/assets/defaultProfile.jpg'),
                      )),
                )
            ),
            ElevatedButton(
                child: Text("Apply"),
                onPressed: () async {
                  User user = await ApiFirebase.getCurrentUser();
                  List data = await ApiFirebase.getDataFromEmail(user.email);
                  List id = await ApiFirebase.getId(user.email);

                  data[0]['user_username'] = userProfileController.usernameController.text;

                  ApiFirebase.UpdateDataUser(id[0], data);

                  print(userProfileController.getAncienneteProfil);
                }),

            //form sign in OS
            Form(
              key: userProfileController.getFormKey,
              child: Column(children: <Widget>[
                TextFormField(
                    controller: userProfileController.usernameController,
                    decoration: const InputDecoration(
                      hintText: 'Enter your username',
                    ),
                    onChanged: (value) { userProfileController.setName = value; },
                    validator: (value) {
                      Pattern username =
                          r'^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$';
                      RegExp regex = new RegExp(username);
                      if (!regex.hasMatch(value)) {
                        return 'Invalid username';
                      } else if (userProfileController.getCheckUsername) {
                        return "username already exist";
                      } else {
                        return null;
                      }
                    }),
                ElevatedButton(
                    child: Text('Inscription OS'),
                    onPressed: () async {
                      userProfileController.setCheckUsername =
                      false; //reset the var before each validator otherwise, the valdiator will never be OK and we can t enter press the submit button again
                      userProfileController.setCheckEmail = false;
                      if (userProfileController.getFormKey.currentState.validate()) {
                        //launch the first validator layer (regex)
                        //_formKey.currentState.save();
                        if (await UtilsFunction.validateUsername(userProfileController.getName) ==
                            false) {
                          //check if username already in use
                          userProfileController.setCheckUsername = true;
                          userProfileController.getFormKey.currentState
                              .validate(); //launch the validator with updated data
                        }
                        if (await UtilsFunction.validateEmail(userProfileController.getEmail) ==
                            false) {
                          //check if email already in use
                          userProfileController.setCheckEmail = true;
                          userProfileController.getFormKey.currentState
                              .validate(); //launch the validator with updated data
                        }
                        if (userProfileController.getCheckUsername == false && userProfileController.getCheckEmail == false) {
                          //insert into database
                          var bytes = utf8.encode(
                              userProfileController.passwordController.text); // data being hashed
                          var digest = sha256.convert(bytes);
                          ApiFirebase.insertDataOS(userProfileController.usernameController.text,
                              digest.toString(), userProfileController.emailController.text);
                          UserCredential userCredential = await FirebaseAuth
                              .instance
                              .createUserWithEmailAndPassword(
                              email: userProfileController.emailController.text,
                              password: userProfileController.passwordController.text);
                        }
                      }

                      //print(emailController.text);
                    }),
              ]),
            )
          ]),
        ));
  }
  Future<Widget> listPopUpChoice(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context)
        =>
            AlertDialog(
              content: new Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  GestureDetector(
                      onTap: () {
                        userProfileController.onImageButtonPressed(
                            ImageSource.camera);
                        Get.back();
                      },
                      child: Text('Nouvelle photo depuis la caméra')
                  ),
                  SizedBox(height: 30),
                  GestureDetector(
                    onTap: () {
                      userProfileController.onImageButtonPressed(
                          ImageSource.gallery);
                      Get.back();
                    },
                    child: Text('Importer depuis la galerie photo'),
                  ),
                  SizedBox(height: 30),
                  GestureDetector(
                    onTap: () {
                      userProfileController.deletePic();
                      Get.back();
                    },
                    child: Text('Supprimer la photo de profil', style: TextStyle(color: Color(Colors.red.value))),
                  )
                ],
              ),
            )
    );
  }
}
