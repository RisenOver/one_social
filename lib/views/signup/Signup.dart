import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:myonesocial/utils/apis/ApiTwitter.dart' as ApiTwitter;
import 'package:myonesocial/utils/general/Utils.dart' as UtilsFunction;
import 'package:myonesocial/utils/apis/ApiFirebase.dart' as ApiFirebase;

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';

import 'package:crypto/crypto.dart';
import 'dart:convert'; // for the utf8.encode method

import 'package:firebase_auth/firebase_auth.dart';


class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  bool checkBoxValue = false;
  bool _isHidden = true;

  bool checkUsername = false;
  bool checkEmail = false;

  String _name;
  String _email;

  var bytes;
  var digest;

  final _formKey = GlobalKey<FormState>();

  TextEditingController emailController = new TextEditingController();
  TextEditingController usernameController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
   return Container(
       child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
          child: Form(
          key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                //Expanded( child:
                //Ajout des champs de saisies
                SizedBox(height: 10.0),
                Container(
                  padding:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 30.0),
                  child: TextFormField(
                    controller: usernameController,
                    decoration: InputDecoration(
                      labelText: 'Identifiant',
                      hintText: 'Entrez votre identifiant',

                      hintStyle: TextStyle(fontSize: 16),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),
                          borderSide: BorderSide(
                            width: 5,
                            style: BorderStyle.none,
                          )),
                    ),
                      onChanged: (value) => _name = value,
                      validator: (value) {
                        Pattern username =
                            r'^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$';
                        RegExp regex = new RegExp(username);
                        if (!regex.hasMatch(value)) {
                          return 'Invalid username';
                        } else if (checkUsername) {
                          return "username already exist";
                        } else {
                          return null;
                        }
                      }
                  ),
                ),
                SizedBox(height: 10.0),
                Container(
                  padding:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 30.0),
                  child: TextFormField(
                    controller: passwordController,
                    obscureText: _isHidden,
                    decoration: InputDecoration(
                      labelText: 'Mot de passe',
                      hintText: 'Entrez votre mot de passe',
                      hintStyle: TextStyle(fontSize: 16),
                      suffix: InkWell(
                        onTap : _togglePasswordView,
                        child: Icon (
                          _isHidden
                              ? Icons.visibility
                              : Icons.visibility_off,
                        ),
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),
                          borderSide: BorderSide(
                            color: Colors.red,
                            width: 5,
                            style: BorderStyle.none,
                          )),
                    ),
                    validator: (value) {
                      Pattern password =
                          r'^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,15}$'; //Expression de correspondance de mot de passe. Le mot de passe doit comporter au moins 4 caractères, pas plus de 8 caractères, et doit inclure au moins une lettre majuscule, une lettre minuscule et un chiffre numérique.
                      RegExp regex = new RegExp(password);
                      if (!regex.hasMatch(value))
                        return 'Invalid password';
                      else
                        return null;
                    },
                   // obscureText: true,
                  ),
                ),
                SizedBox(height: 10.0),
                Container(
                  padding:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 30.0),

                  child: TextFormField(
                    controller: emailController,
                    decoration: InputDecoration(
                      labelText: 'Email',
                      hintText: 'Entrez votre email',
                      hintStyle: TextStyle(fontSize: 16),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),
                          borderSide: BorderSide(
                            width: 5,
                            style: BorderStyle.none,
                          )),
                    ),
                      onChanged: (value) => _email = value,
                      validator: (value) {
                        if (!RegExp(
                            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                            .hasMatch(value))
                          return 'Invalid email';
                        else if (checkEmail) {
                          return ("Email already exist");
                        } else
                          return null;
                      }
                  ),
                ),
                Container(
                    padding:
                        EdgeInsets.symmetric(vertical: 0.0, horizontal: 30.0),
                    child: Row(
                      children: [
                        Checkbox(
                            value: checkBoxValue,
                            onChanged: (bool value) {
                              print(value);

                              setState(() {
                                checkBoxValue = value;
                              });
                            }),
                        Text("J'accepte les",
                            style: TextStyle(
                              fontSize: 13,
                              fontFamily: 'Roboto Regular',
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                            )),
                        FlatButton(
                          textColor: Colors.deepPurple[300],
                          child: Text(
                            'Termes & Conditions',
                            style: TextStyle(
                              fontSize: 13,
                              fontFamily: 'Roboto Regular',
                            ),
                          ),
                          onPressed: () {
                            //Lien renvoyant vers l'inscription
                          },
                        )
                      ],
                    )),
                SizedBox(height: 10.0),
                //Ajout du button d'inscription
                Container(
                  padding:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 30.0),
                  child: FlatButton(
                    onPressed: () async {
                      checkUsername = false; //reset the var before each validator otherwise, the valdiator will never be OK and we can t enter press the submit button again
                      checkEmail = false;
                      if (_formKey.currentState.validate()) {
                        //launch the first validator layer (regex)
                        //_formKey.currentState.save();
                        if (await UtilsFunction.validateUsername(_name) ==
                            false) {
                          //check if username already in use
                          checkUsername = true;
                          _formKey.currentState
                              .validate(); //launch the validator with updated data
                        }
                        if (await UtilsFunction.validateEmail(_email) ==
                            false) {
                          //check if email already in use
                          checkEmail = true;
                          _formKey.currentState
                              .validate(); //launch the validator with updated data
                        }
                        if (checkUsername == false && checkEmail == false) {
                          //insert into database
                          bytes = utf8.encode(
                              passwordController.text); // data being hashed
                          digest = sha256.convert(bytes);
                          ApiFirebase.insertDataOS(usernameController.text,
                              digest.toString(), emailController.text);
                          UserCredential userCredential = await FirebaseAuth
                              .instance
                              .createUserWithEmailAndPassword(
                              email: emailController.text,
                              password: passwordController.text);
                        }
                      }

                      //print(emailController.text);
                    },
                    height: 40.0,
                    color: Colors.deepPurple[300],
                    child: Text('INSCRIPTION',
                        style: TextStyle(
                            fontFamily: 'Roboto Bold',
                            color: Colors.white,
                            fontSize: 20)),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(60.0)),
                  ),
                ),
              ],
            ),
          ),
        ),
       )
   );
        }

  void _togglePasswordView() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }
}
