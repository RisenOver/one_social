import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:myonesocial/controllers/UserProfileController.dart';
import 'package:get/get.dart';

class PartiEnd extends StatelessWidget {
  final UserProfileController userProfileController = Get.put(UserProfileController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        padding: EdgeInsets.symmetric(vertical: 90.0, horizontal: 20.0),
        //ajout du nom de l'utilisateur et de l'icone edite son profil
        children: <Widget>[
          Obx(()=>RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: userProfileController.getName,
                  style: TextStyle(
                    shadows: [
                      Shadow(
                        blurRadius: 10.0,
                        color: Color.fromARGB(60, 20, 23, 26),
                        // color: Colors.grey.withOpacity(0.3),
                        offset: Offset(5.0, 5.0),
                      ),
                    ],
                    fontFamily: 'Roboto Regular',
                    color: Colors.deepPurple[300],
                    fontSize: 24,
                  ),
                ),
                WidgetSpan(
                  child: Icon(FontAwesomeIcons.userEdit,
                      color: Colors.deepPurple[300], size: 18),
                ),
              ],
            ),
          )),
//ajout de la phrase memebre depuis un an
          Container(
              child: Obx(()=>Text(
            'Membre depuis  ${userProfileController.getAncienneteProfil}',
            style: TextStyle(
              shadows: [
                Shadow(
                  blurRadius: 10.0,
                  color: Color.fromARGB(50, 20, 23, 26),
                  // color: Colors.grey.withOpacity(0.3),
                  offset: Offset(5.0, 5.0),
                ),
              ],
              fontFamily: 'Roboto Light',
              color: Colors.deepPurple[300],
              fontSize: 11,
            ),
          ))),
          SizedBox(height: 4),
          //ajout de phrase
          Container(
              padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 0.0),
              child: Text(
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ed do eiusmod tempor incididunt',
                //Ajout du sous-titre
                style: TextStyle(
                  fontFamily: 'Roboto Light',
                  color: Color(0xFF14171A),
                  fontSize: 14,
                ),
              )),
//ajout de la phrase mes reseaux sociaux ainsi que de l'icone ajouter
          Row(
            children: <Widget>[
              Expanded(
                child: ListTile(
                    title: Text(
                      'Mes réseaux sociaux',
                      style: TextStyle(
                        fontFamily: 'Roboto Light',
                        color: Color(0xFF14171A),
                        fontSize: 22,
                      ),
                    ),
                    trailing: Wrap(
                      spacing: 12, // space between two icons
                      children: <Widget>[
                        Icon(Icons.add_circle,
                            color: Colors.deepPurple[300], size: 25), // icon-1
                      ],
                    ),
                    // shape: new CircleBorder(),
                    onTap: () => print("ListTile")),
              ),
            ],
          ),
//ajout des ' reseaux sociaux plus leurs statuts
          //LIGNE 1
          Row(
            children: [
              Expanded(
                  child: new Card(
                elevation: 2,
                child: ListTile(
                    leading: Icon(FontAwesomeIcons.youtube, color: Colors.red),
                    title: Text(
                      'Tom',
                      style: TextStyle(
                        fontFamily: 'Roboto Light',
                        color: Colors.deepPurple[300],
                      ),
                    ),
                    trailing: Wrap(
                      spacing: 12, // space between two icons
                      children: <Widget>[
                        Icon(Icons.brightness_1_rounded,
                            color: Colors.green, size: 5), // icon-1
                      ],
                    ),
                    // shape: new CircleBorder(),
                    onTap: () => print("ListTile")),
              )),
              Expanded(
                  child: new Card(
                child: ListTile(
                    leading: Icon(FontAwesomeIcons.youtube, color: Colors.red),
                    title: Text(
                      'Tom',
                      style: TextStyle(
                        fontFamily: 'Roboto Light',
                        color: Colors.deepPurple[300],
                      ),
                    ),
                    trailing: Wrap(
                      spacing: 12, // space between two icons
                      children: <Widget>[
                        Icon(Icons.brightness_1_rounded,
                            color: Colors.green, size: 5), // icon-1
                      ],
                    ),
                    // shape: new CircleBorder(),
                    onTap: () => print("ListTile")),
              ))
            ],
          ),
//LIGNE 2
          Row(
            children: [
              Expanded(
                  child: new Card(
                elevation: 2,
                child: ListTile(
                    leading: Icon(
                      FontAwesomeIcons.instagram,
                      color: Color.fromRGBO(138, 58, 185, 1),
                    ),
                    title: Text(
                      'Tom',
                      style: TextStyle(
                        fontFamily: 'Roboto Light',
                        color: Colors.deepPurple[300],
                      ),
                    ),
                    trailing: Wrap(
                      spacing: 12, // space between two icons
                      children: <Widget>[
                        Icon(Icons.brightness_1_rounded,
                            color: Colors.green, size: 5), // icon-1
                      ],
                    ),
                    // shape: new CircleBorder(),
                    onTap: () => print("ListTile")),
              )),
              Expanded(
                  child: new Card(
                child: ListTile(
                    leading: Icon(
                      FontAwesomeIcons.twitch,
                      color: Color.fromRGBO(100, 65, 165, 1),
                    ),
                    title: Text(
                      'Tom',
                      style: TextStyle(
                        fontFamily: 'Roboto Light',
                        color: Colors.deepPurple[300],
                      ),
                    ),
                    trailing: Wrap(
                      spacing: 12, // space between two icons
                      children: <Widget>[
                        Icon(Icons.brightness_1_rounded,
                            color: Colors.green, size: 5), // icon-1
                      ],
                    ),
                    // shape: new CircleBorder(),
                    onTap: () => print("ListTile")),
              ))
            ],
          ),
        ],
      ),
    );
  }
}
