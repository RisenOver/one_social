import 'package:get/get.dart';

/// N'hésitez pas à regarder la documentation de Get : https://pub.dev/packages/get
/// la doc complète sur le State Management : https://github.com/jonataslaw/getx/blob/master/documentation/en_US/state_management.md
/// la doc complète sur le Route Management : https://github.com/jonataslaw/getx/blob/master/documentation/en_US/route_management.md
/// la doc complète sur le Dependency Management : https://github.com/jonataslaw/getx/blob/master/documentation/en_US/dependency_management.md

/*
  Cette classe controller contient toutes les variables et fonctions
   utilisables par la vue (les Dev front).
   Pour les rendre accessible dans la vue, il faut injecter ce controller dans
   celle-ci (voir dans views/ExampleView.dart comment j'ai fais)
 */
class ExampleController extends GetxController {

  /*
  Avec le plugin Get, il est possible de rendre une variable "observable".
  Cela signifie qu'à chaque fois que ce type de variable change, toutes les autres
  variables et/ou widgets visuels qui l'utilisent vont aussi changer.

  Par exemple :
  List<dynamic> nombreDeLanguages = ["python", "java", "C"]

  On rend cette variable observable en ajoutant à la fin ".obs"
  Ce qui donne :

  List nombreDeLanguages = ["python", "java", "C"].obs

  Si dans la vue on a un listing de cette liste, alors à chaque fois que
  nombreDeLangages change (remove,add ou update un item), le listing dans la vue
  va aussi changer car elle "observe" nombreDeLanguages

  Remarque : s'il s'agit d'une liste qui reçoit des string "dans le futur",
  alors on écrira RxList et pas List

  Pour les autres type de variables, c'est aussi possible !

  final name = ''.obs;
  final isLogged = false.obs;
  final count = 0.obs;
  final balance = 0.0.obs;
  final number = 0.obs;
  final items = <String>[].obs;
  final myMap = <String, int>{}.obs;

  // Aussi possible avec des objets !
  final user = User().obs;

   */

  // Une variable qui peut être "observée"
  List nombreDeLanguages = ["python", "java", "C"].obs;

  // Get nous fournit des "super" fonctions qui peuvent être utilisées durant
  // le cycle de vie du controller
  // Lorsque le controller s'initialise : onInit()
  // Lorsqu'on veut le détruire : onClose() (mais Get se charge de le détruire
  // et sait quand le faire donc utilisez la quand c vraiment nécessaire)

  // Example concret
  RxString nom = ''.obs;

  get getNom => this.nom.value;


  @override
  void onInit() {
    //fetchApi();
    super.onInit();
  }

  // Des fonctions qui peuvent être appelée dans la vue
  List getList() {
    // blablabla...
  }

  void setList(List newlist) {
    // blablabla...
  }

  void addList(String newItem) {
    // blablabla...
  }

  void updateList(String item) {
    // blablabla...
  }

  void removeList() {
    // blablabla...
  }
}