import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_session/flutter_session.dart';
import 'package:get/get.dart';
import '../utils/apis/ApiFirebase.dart' as ApiFirebase;
import '../utils/general/Utils.dart' as UtilsFunctions;

class SignUpController extends GetxController {

  bool _checkBoxValue = false;
  bool _isHidden = true;

  RxBool _checkUsername = false.obs;
  RxBool _checkEmail = false.obs;

  RxString _name = ''.obs;
  RxString _email = ''.obs;

  final _formKey = GlobalKey<FormState>();

  Rx<TextEditingController> emailController = TextEditingController().obs;
  Rx<TextEditingController> usernameController = TextEditingController().obs;
  Rx<TextEditingController> passwordController = TextEditingController().obs;

  get getIsHidden => this._isHidden;
  get getCheckBoxValue => this._checkBoxValue;

  get getCheckUsername => this._checkUsername.value;
  get getCheckEmail => this._checkEmail.value;
  get getName => this._name.value;
  get getEmail => this._email.value;
  get getFormKey => this._formKey;
  get getEmailController => this.emailController.value;
  get getUsernameController => this.usernameController.value;
  get getPasswordController => this.passwordController.value;

  set setCheckUsername(bool newCheckUsername) => this._checkUsername.value = newCheckUsername;
  set setCheckEmail(bool newCheckEmail) => this._checkEmail.value = newCheckEmail;
  set setName(String newName) => this._name.value = newName;
  set setEmail(String newEmail) => this._email.value = newEmail;

  void changeCheckBoxValue(bool newCheckBoxValue) {
    this._checkBoxValue = newCheckBoxValue;
    update();
  }

  void togglePasswordView() {
    this._isHidden = !this._isHidden;
    update();
  }

  void signUp() async {
    this._checkUsername.value = false; //reset the var before each validator otherwise, the valdiator will never be OK and we can t enter press the submit button again
    this._checkEmail.value = false;
    if (this.getFormKey.currentState.validate()) {
      //launch the first validator layer (regex)
      //this.getFormKey.currentState.save();
      if (await UtilsFunctions.validateUsername(this.getName) ==
          false) {
        //check if username already in use
        this._checkUsername.value = true;
        this.getFormKey.currentState
            .validate(); //launch the validator with updated data
      }
      if (await UtilsFunctions.validateEmail(this.getEmail) ==
          false) {
        //check if email already in use
        this._checkEmail.value = true;
        this.getFormKey.currentState
            .validate(); //launch the validator with updated data
      }
      if (this.getCheckUsername == false && this.getCheckEmail == false) {
        //insert into database
        var bytes = utf8.encode(
            this.getPasswordController.text); // data being hashed
        var digest = sha256.convert(bytes);
        ApiFirebase.insertDataOS(this.getUsernameController.text,
            digest.toString(), this.getEmailController.text);
        UserCredential userCredential = await FirebaseAuth
            .instance
            .createUserWithEmailAndPassword(
            email: this.getEmailController.text,
            password: this.getPasswordController.text);
        var session = FlutterSession();
        await session.set("email", this.getEmailController.text);
      }
    }
  }
}
