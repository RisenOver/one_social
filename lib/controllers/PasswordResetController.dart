import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:myonesocial/utils/apis/ApiFirebase.dart' as ApiFirebase;
class PasswordResetController extends GetxController {

  TextEditingController email = new TextEditingController();

  get getEmail => this.email;

  void sendPasswordReset()
  {
    print("Message envoyé à l'email suivant ${getEmail.text}");
    ApiFirebase.sendPasswordReset(this.getEmail.text);
  }

}