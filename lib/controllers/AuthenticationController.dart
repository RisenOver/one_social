import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthenticationController extends GetxController with SingleGetTickerProviderMixin{

  RxString email = ''.obs;
  TabController _tabController;

  @override
  void onInit() {
    super.onInit();
    this._tabController = TabController(length: 2, vsync: this);
    getPreferences();
  }

  getPreferences() async { // access to preference
    SharedPreferences preferences = await SharedPreferences.getInstance();
    if(preferences.containsKey('email') && preferences.getString('email').isNotEmpty) {
      setEmail = preferences.getString('email');
    }
  }

  get getEmail => this.email.value;
  get getTabController => this._tabController;
  set setEmail(String newEmail) => this.email.value = newEmail;
}
