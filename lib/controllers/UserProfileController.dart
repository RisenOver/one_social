import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import '../utils/apis/ApiFirebase.dart' as ApiFirebase;
import '../utils/general/Utils.dart' as UtilsFunctions;
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:myonesocial/controllers/InfoUserController.dart';
import 'dart:developer' as developer;
import 'package:flutter_session/flutter_session.dart';

class UserProfileController extends GetxController {


  RxBool _checkUsername = false.obs;
  RxBool _checkEmail = false.obs;

  RxString _name = ''.obs;
  RxString _email = ''.obs;

  RxString _ancienneteProfil = ''.obs;

  final _formKey = GlobalKey<FormState>();

  TextEditingController emailController = new TextEditingController();
  TextEditingController usernameController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  get getCheckUsername => this._checkUsername.value;
  get getCheckEmail => this._checkEmail.value;
  get getName => this._name.value;
  get getEmail => this._email.value;
  get getFormKey => this._formKey;
  get getEmailController => this.emailController.value;
  get getUsernameController => this.usernameController.value;
  get getAncienneteProfil => this._ancienneteProfil.value;

  set setCheckUsername(bool newCheckUsername) => this._checkUsername.value = newCheckUsername;
  set setCheckEmail(bool newCheckEmail) => this._checkEmail.value = newCheckEmail;
  set setName(String newName) => this._name.value = newName;
  set setEmail(String newEmail) => this._email.value = newEmail;
  set setAncienneteProfil(String newValue) => this._ancienneteProfil.value = newValue;

  final InfoUserController infoUserController = Get.put(InfoUserController());

  final ImagePicker _picker = ImagePicker();

  Rx<PickedFile> _imageFile = PickedFile('').obs;
  RxString _image = ''.obs;
  RxString _pickImageError = ''.obs;
  RxString idUser = ''.obs;

  get getImageFile => this._imageFile.value.path;
  get getIdUser => this.idUser.value;
  get getImage => this._image.value;
  get getPickImageError => this._pickImageError.value;



  @override
  void onInit() {
    super.onInit();
    initData();
    initImage();
  }

  Future initImage() async {
    User user = await ApiFirebase.getCurrentUser();
    List data = await ApiFirebase.getId(user.email);
    this.idUser.value = data[0];

    var ref = FirebaseStorage.instance.ref().child('profilePics/${this.idUser.value}');
    var loc = await ref.getDownloadURL();
    this._image.value = loc != '' ? loc : '';
  }

  Future <void> initData() async{
    User user = await ApiFirebase.getCurrentUser();
    List data = await ApiFirebase.getDataFromEmail(user.email);
    this.setName = data[0]['user_username'];
    usernameController.text = this.getName;
    var dateInscription = data[0]['user_dateCreation'].millisecondsSinceEpoch;
    initAnciennete(dateInscription);
  }

  Future <void> initAnciennete(dateInscription) async{

    var date = DateTime.fromMillisecondsSinceEpoch(dateInscription);
    var today = DateTime.now();
    var inscriptionUser = DateTime.utc(date.year, date.month, date.day);
    Duration difference = today.difference(inscriptionUser);
    var ecart;

    //print(difference.inDays);
    if(difference.inDays > 365 ){
      ecart = today.year - date.year;
      this.setAncienneteProfil="$ecart an(s)";
    }
    else if(difference.inDays > 30 ){
      ecart = (today.month - date.month).abs();
      this.setAncienneteProfil="$ecart Mois";
    }
    else{
      ecart = difference.inDays;
      this.setAncienneteProfil="$ecart jour(s)";
    }
  }

  void onImageButtonPressed(ImageSource source) async {

    // On demande à l'user de choisir une photo dans sa gallerie d'images
    // ou prendre une photo avec sa camera
    try {
      final pickedFile = await _picker.getImage(
        source: source,
      );

      this._imageFile.value = pickedFile;
      await uploadImageToFirebase();

    } catch (e) {
      this._pickImageError.value = 'Impossible de changer votre photo de profil. Réessayez.';
    }
  }


  Future uploadImageToFirebase() async {
    //String fileName = basename(_imageFile.value.path);
    try {

      dynamic email = await FlutterSession().get("email");
      await FirebaseFirestore.instance
          .collection('users')
          .get().then((doc) => {
        doc.docs.forEach((doc) async {
          if (doc.get("user_email") == email) {
            print(email);
            this.idUser.value = doc.id;
            print(this.idUser.value);
            var ref = FirebaseStorage.instance.ref().child('profilePics/${this.idUser.value}');
            await ref.putFile(File(_imageFile.value.path));
            var loc = await ref.getDownloadURL();
            this._image.value = loc;
            print(loc);
            print(this._image.value);
          }

        })
      });
      developer.log('ensu');


      developer.log('ca mépuise la');

      developer.log(this.idUser.value);


    } on FirebaseException catch (e) {

    }
  }

  void deletePic() async {
    await FirebaseStorage.instance.ref().child('profilePics/${this.idUser.value}').delete();
    this._image.value = '';
  }
}
