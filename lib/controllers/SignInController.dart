import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class SignInController extends GetxController {

  RxBool checkRemember = false.obs;
  RxBool isHidden = true.obs;

  Rx<TextEditingController> identifiantControllers = TextEditingController().obs;
  Rx<TextEditingController> passwordControllers = TextEditingController().obs;

  get getCheckRemember => this.checkRemember.value;
  set setCheckRemember(bool newCheckRemember) => this.checkRemember.value = newCheckRemember;

  get getIsHidden => this.isHidden.value;
  set setIsHidden(bool newIsHidden) => this.isHidden.value = newIsHidden;

  get getIdentifiantControllers => this.identifiantControllers.value;
  get getPasswordControllers => this.passwordControllers.value;

  void changeCheckMember(bool newVal) {
    this.checkRemember.value = newVal;
  }
}