import 'package:get/get.dart';

class InfoUserController extends GetxController {

  RxString email = ''.obs;

  get getEmail => this.email.value;
  set setEmail(String newEmail) => this.email.value = newEmail;

}
