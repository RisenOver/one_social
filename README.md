# onesocial

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

DEPLOY URL : https://onesocial-99dd4.web.app

================================================
PROEJECT'S ARCHITECTURE
```
onesocial
└─── lib
     └─── controllers/ --> Mis à disposition une fois hydratés pour être injectés dans le front
     |       Flow.dart
     |       Settings.dart
     |
     └─── models/  --> Les objets à hydrater via les requêtes API
     |    └───Posts/
     |    |       Post.dart
     |    |       PostTwitter.dart
     |    |       PostFacebook.dart
     |    └─── Feeds/
     |    |       FeedTwitter.dart
     |    |       FeedFacebook.dart
     |    └─── User/
     |    |       UserOS.dart
     |    |       UserTwitter.dart
     |    |       Medias.dart
     |
     └─── utils/  --> Fonctions et constantes générales pouvant être appelées de partout
     |    └─── APIs/
     |    |       ApiTwitter.dart
     |    |       ApiFacebook.dart
     |    |       ApiFirebase.dart
     |    └─── general/
     |     // Je sais pas ce qui pourrait aller là, mais bref le reste tmtc
     |
     └─── views/  --> Pages globales
     |    └─── Feeds/
     |    |       Flow.dart
     |    |       FeedSettings.dart
     |    └─── Settings/
     |    |       UserSettings.dart
     |    |       MediasSettings.dart
     |    └─── Signup/
     |    |       Signup.dart
     |    |       PasswordReset.dart
     |
     └─── components/  --> Morceaux de code qui reviennent souvent (inputs, boutons)
     |    └─── Posts/
     |    |       PostTwitter.dart
     |    |       PostFacebook.dart
     |    └─── Inputs/
     |    |       InputText.dart
     |    |       InputCheckbox.dart
     |    |       InputTextarea.dart
     |    |    AppLogo.dart
     |
     └─── assets/  --> Images, ...
     |    main.dart
     |    routes.dart
```